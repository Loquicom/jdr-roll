var first = true;
var uniqId = '0';

$(document).ready(function(){

	//Ini Materialize
    M.AutoInit();

    //Si message à afficher
    if($("#msg").val().trim() != ''){
    	var msg = '';
    	switch($("#msg").attr('data-type')){
    		case 'err':
    			msg = message_erreur($("#msg").val());
    			break;
    		case 'ok':
    			msg = message_conf($("#msg").val());
    			break;
    		default:
    			msg = message_info($("#msg").val());
    	}
    	float_message(msg);
    }

    //Envoyer formulaire
    $("#valid").on('click', function(){
        //Verif form
    	if($('#save-name').val() == null && $("#name").val().trim() == ''){
    		float_message(message_erreur('Lanceur vide'));
    		return false;
    	}
        //Envoi
        $.post('ajx/roll.php', {name: $("#name").val(), 'save-name': $('#save-name').val(), roll: $("#roll").val(), hide: $("#hide").prop('checked')}, function(data){
            if(data.etat = 'ok'){
                float_message(message_info('Resultat : ' + data.roll + ' / ' + data.dice));
                reload(false);
                $("#name").val('');
            }
        }, 'json');
    	return false;
    });

    //Rechargement
    $(".reload").on('click', function(){
        if(first){
            reload(false);
            first = false;
        } else {
            reload(true);
        }   
    }).click();
    //Recharge toutes les X secondes
    perma_check();

    //Reset
    $('.reset').on('click', function(){
    	reset_score();
    });

    //Quit
    $('.quit').on('click', function(){
    	location.href = './?quit';
    });

    $("body").on('click', '.float_message', function () {
        $(this).slideUp('slow', function () {
            $(this).html('');
        });
    });

});

function check(){
    $.post("ajx/check.php", {'id': uniqId}, function(data){
        if(data.etat == 'ok'){
            if(data.update){
                reload(false);
            }
        }
    }, 'json');
}

function perma_check(time){
    time = time || 15;
    setTimeout(function(){
        check();
        perma_check(time);
    }, time * 1000);
}

function reload(msg){
    $.post("ajx/score.php", null, function(data){
        if(data.etat == 'ok'){
            uniqId = data.uniqId;
            $("#tbody").html(data.html);
            $("#save-name").html(data.savename);
            if(msg){
                float_message(message_conf("Page recharg&eacute;e"));
            }
            M.AutoInit();
        }
    }, 'json');
}

function reset_score(){
    $.post("ajx/reset.php", null, function(data){
        if(data.etat == 'ok'){
            $("#tbody").html('');
            $("#save-name").html('<option value="" disabled selected>Selectionnez un lanceur</option>');
            float_message(message_conf("Session reset"));
            M.AutoInit();
        }
    }, 'json');
}

function message_erreur(msg){
    var html = '';
    html += '<div class="hide-on-small-only">' + "\n";
    html += '   <div class="row red-text red lighten-4" style="height: 60px; border: dashed;">' + "\n";
    html += '       <div class="col s2">' + "\n";
    html += '           <i class="material-icons" style="font-size: 3em; line-height: 125%">error</i>' + "\n";
    html += '       </div>' + "\n";
    html += '       <div class="col s10 truncate " style="font-size: 1.5em; line-height: 250%">' + "\n";
    html += '           ' + msg + "\n";
    html += '       </div>' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    html += '<div class="hide-on-med-and-up">' + "\n";
    html += '   <div class="red lighten-4 red-text center" style="border: dashed">' + "\n";
    html += '       <br>' + msg + '<br>&nbsp;' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    
    return html;
}

function message_info(msg){
    var html = '';
    html += '<div class="hide-on-small-only">' + "\n";
    html += '   <div class="row blue-text blue lighten-4" style="height: 60px; border: dashed;">' + "\n";
    html += '       <div class="col s2">' + "\n";
    html += '           <i class="material-icons" style="font-size: 3em; line-height: 125%">info</i>' + "\n";
    html += '       </div>' + "\n";
    html += '       <div class="col s10 truncate " style="font-size: 1.5em; line-height: 250%">' + "\n";
    html += '           ' + msg + "\n";
    html += '       </div>' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    html += '<div class="hide-on-med-and-up">' + "\n";
    html += '   <div class="blue lighten-4 blue-text center" style="border: dashed">' + "\n";
    html += '       <br>' + msg + '<br>&nbsp;' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    
    return html;
}

function message_warn(msg){
    var html = '';
    html += '<div class="hide-on-small-only">' + "\n";
    html += '   <div class="row amber-text yellow lighten-4" style="height: 60px; border: dashed;">' + "\n";
    html += '       <div class="col s2">' + "\n";
    html += '           <i class="material-icons" style="font-size: 3em; line-height: 125%">warning</i>' + "\n";
    html += '       </div>' + "\n";
    html += '       <div class="col s10 truncate " style="font-size: 1.5em; line-height: 250%">' + "\n";
    html += '           ' + msg + "\n";
    html += '       </div>' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    html += '<div class="hide-on-med-and-up">' + "\n";
    html += '   <div class="yellow lighten-4 amber-text center" style="border: dashed">' + "\n";
    html += '       <br>' + msg + '<br>&nbsp;' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    
    return html;
}

function message_conf(msg){
    var html = '';
    html += '<div class="hide-on-small-only">' + "\n";
    html += '   <div class="row green-text green lighten-4" style="height: 60px; border: dashed;">' + "\n";
    html += '       <div class="col s2">' + "\n";
    html += '           <i class="material-icons" style="font-size: 3em; line-height: 125%">check_circle</i>' + "\n";
    html += '       </div>' + "\n";
    html += '       <div class="col s10 truncate " style="font-size: 1.5em; line-height: 250%">' + "\n";
    html += '           ' + msg + "\n";
    html += '       </div>' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    html += '<div class="hide-on-med-and-up">' + "\n";
    html += '   <div class="green lighten-4 green-text center" style="border: dashed">' + "\n";
    html += '       <br>' + msg + '<br>&nbsp;' + "\n";
    html += '   </div>' + "\n";
    html += '</div>' + "\n";
    
    return html;
}

function float_message(text){
    $("#float_msg").html(text).slideDown('slow', null);
    setTimeout(function(){
        $("#float_msg").click();
    }, '5000');
}