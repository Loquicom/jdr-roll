<?php

//Recup de la session
if(!isset($_COOKIE['salon']) || !isset($_COOKIE['common-sess'])){
	exit(json_encode(['etat' => 'err']));
}
if($_COOKIE['common-sess'] != $_COOKIE['salon']){
	exit(json_encode(['etat' => 'err']));
}

//Lancement session
session_name('common-sess');
session_start();

//Generation tableau
$roll = array_reverse($_SESSION['roll']);
$html = '';
foreach ($roll as $val) {
	$html .= '<tr><td>' . $val['name'] . '</td>';
	$html .= '<td>' . $val['roll'] . '</td>';
	$html .= '<td>' . $val['dice'] . '</td></tr>';
}

//Generation nom joueur
$name = '<option value="" disabled selected>Selectionnez un lanceur</option>';
foreach ($_SESSION['user'] as $val) {
	$name .= '<option>' . $val . '</option>';
}

//Envoi
echo json_encode(['etat' => 'ok', 'html' => $html, 'savename' => $name, 'uniqId' => count($_SESSION['roll'])]);