<?php

//Recup de la session
if(!isset($_COOKIE['salon']) || !isset($_COOKIE['common-sess'])){
	exit(json_encode(['etat' => 'err']));
}
if($_COOKIE['common-sess'] != $_COOKIE['salon']){
	exit(json_encode(['etat' => 'err']));
}

//Lancement session
session_name('common-sess');
session_start();

//Compare les ids
if(!isset($_POST['id'])){
	exit(json_encode(['etat' => 'erre']));
}
$json = ['etat' => 'ok', 'update' => false];
if($_POST['id'] != count($_SESSION['roll'])){
	$json['update'] = true;
	$_SESSION['id'] = $_POST['id'];
}
echo json_encode($json);