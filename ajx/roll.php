<?php

//Recup de la session
if(!isset($_COOKIE['salon']) || !isset($_COOKIE['common-sess'])){
	exit(json_encode(['etat' => 'err']));
}
if($_COOKIE['common-sess'] != $_COOKIE['salon']){
	exit(json_encode(['etat' => 'err']));
}

//Lancement session
session_name('common-sess');
session_start();

//Traitement formulaire
$json = [];
if(isset($_POST['roll'])){
	//Regarde si l'utilisateur est nouveau
	$name = '';
	if(isset($_POST['name']) && trim($_POST['name']) != ''){
		$name = $_POST['name'];
		if(!isset($_SESSION['user'][$name])){
			$_SESSION['user'][$name] = $name;
		}
	} else if(isset($_POST['save-name'])){
		$name = $_POST['save-name'];
	} else {
		$type = 'err';
		$value = 'Aucun lanceur';
	}
	//Si il y a un bien un utilisateur
	if($name != ''){
		//Tirage
		$roll = mt_rand(1, $_POST['roll']);
		//Affichage
		if(!isset($_POST['hide'])){
			$_SESSION['roll'][] = [
				'name' => $name,
				'roll' => $roll,
				'dice' => $_POST['roll']
			];
		}
		$json['roll'] = $roll;
		$json['dice'] = $_POST['roll'];
		$json['etat'] = 'ok';
	}
} else {
	$json['etat'] = 'err';
}

echo json_encode($json);