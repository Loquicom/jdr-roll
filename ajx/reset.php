<?php

//Recup de la session
if(!isset($_COOKIE['salon']) || !isset($_COOKIE['common-sess'])){
	exit(json_encode(['etat' => 'err']));
}
if($_COOKIE['common-sess'] != $_COOKIE['salon']){
	exit(json_encode(['etat' => 'err']));
}

//Lancement session
session_name('common-sess');
session_start();

//Vide la session
$_SESSION['user'] = [];
$_SESSION['roll'] = [];

//Retour
echo json_encode(['etat' => 'ok']);