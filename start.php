<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>JdR</title>
    </head>

    <body class="grey lighten-4">

    	<style type="text/css">
    		.center{
    			text-align: center;
    		}

    		.v-center {
  				margin-top: 50vh; /* poussé de la moitié de hauteur de viewport */
  				transform: translateY(-50%); /* tiré de la moitié de sa propre hauteur */
			}
    	</style>

    	<main class="container">
    		<div class="v-center">
    			<form method="POST" action="./">
	    			<div class="row center">
	    				<div class="col s12">
	    					<h3>Nom du salon</h3>
	    					<input type="text" name="salon" autocomplete="off">
	    					<button type="submit" class="waves-effect waves-light btn">Rejoindre</button>
	    				</div>
	    			</div>
    			</form>
    		</div>
    	</main>

    	<div id="float_msg" class="float_message" style="display: none;"></div>

    	<!--JavaScript at end of body for optimized loading-->
    	<script type="text/javascript" src="js/materialize.min.js"></script>
    	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
	</body>
</html>