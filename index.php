<?php

//Regarde si il y a un cookie avec le nom de la session à charger ou l'info en POST
if(isset($_POST['salon'])){
	//Création du cookie
	setcookie('salon', $_POST['salon'], time() + 60 * 60 * 24, '/', '', false, true);
	//Recharge la page
	header('Location: ./');
	exit;
} else if(!isset($_COOKIE['salon'])){
	require 'start.php';
	exit;
}

//Activation de la session
if(!isset($_COOKIE['common-sess'])){
	//Création du cookie de session commun
	setcookie('common-sess', $_COOKIE['salon'], time() + 60 * 60 * 24, '/', '', false, true);
	//Recharge la page
	header('Location: ./');
	exit;
} else if($_COOKIE['common-sess'] != $_COOKIE['salon']){
	//Changement de la valeur de la session
	setcookie('common-sess', $_COOKIE['salon'], time() + 60 * 60 * 24, '/', '', false, true);
	//Recharge la page
	header('Location: ./');
	exit;
}

//Lancement session
session_name('common-sess');
session_start();

//Valeur message
$type = '';
$value = '';

//Param Get
if(isset($_GET['quit'])){
	setcookie('salon', '', 0, '/', '', false, true);
	header('Location: ./');
	exit;
}
if(isset($_GET['type']) && isset($_GET['value'])){
	$type = $_GET['type'];
	$value = $_GET['value'];
}

//Setup session si besoins
if(!( isset($_SESSION['user']) && isset($_SESSION['roll']) )){
	$_SESSION['user'] = [];
	$_SESSION['roll'] = [];
	$_SESSION['id'] = '0';
}

//Page web
require 'header.php';
?>

<style type="text/css">
	.center{
		text-align: center;
	}

	.float_message {
    	position: absolute;	
    	top: 120px;
    	right: 80px;
    	z-index: 100;
    	box-shadow:  12px 12px 2px 1px rgba(0, 0, 255, .2);
    	margin:  0 0 0 0;
    	height: 60px;
    	cursor:  pointer;
	}

	@media only screen and (min-width: 601px){
		.float_message{
			width: 800px;
		}
	}

	@media only screen and (max-width: 600px){
		.float_message{
			width: 245px;
		}
	}
</style>

<div class="hide-on-small-only">
	<div style="position: absolute; top: 40px; left: 80px; cursor: pointer;">
		<i class="quit material-icons tooltipped" data-position="right" data-tooltip="Changer de salon" style="font-size: 4em">arrow_back</i>
	</div>

	<div style="position: absolute; top: 40px; right: 80px; cursor: pointer;">
		<i class="reload material-icons tooltipped" data-position="bottom" data-tooltip="Recharger la page" style="font-size: 4em">refresh</i>
	</div>

	<div style="position: absolute; top: 40px; right: 160px; cursor: pointer;">
		<i class="reset material-icons tooltipped" data-position="left" data-tooltip="Reset les lancer" style="font-size: 4em">settings_backup_restore</i>
	</div>
</div>
<div class="hide-on-med-and-up">
	<div style="position: absolute; top: 100px; left: 80px; cursor: pointer;">
		<i class="quit material-icons tooltipped" data-position="right" data-tooltip="Changer de salon" style="font-size: 4em">arrow_back</i>
	</div>

	<div style="position: absolute; top: 100px; right: 80px; cursor: pointer;">
		<i class="reload material-icons tooltipped" data-position="bottom" data-tooltip="Recharger la page" style="font-size: 4em">refresh</i>
	</div>

	<div style="position: absolute; top: 100px; right: 160px; cursor: pointer;">
		<i class="reset material-icons tooltipped" data-position="left" data-tooltip="Reset les lancer" style="font-size: 4em">settings_backup_restore</i>
	</div>
</div>

<main class="container">
	<div class="row">
		<div class="col s12 center">
			<h1><b>JdR Roll</b></h1>
		</div>
	</div>
	<form id="form" method="POST" action="./" style="margin-top: 1em">
		<div class="row">
			<div class="col m6 s12">
				<div class="row">
		        	<div class="input-field col s12">
		          		<input id="name" type="text" name="name">
		          		<label for="name">Nom du lanceur</label>
		        	</div>
		      	</div>
		    </div>
		    <div class="col m6 s12">
				<div class="row">
		        	<div class="input-field col s12">
		          		<select id="save-name" name="save-name">
		    			</select>
		    			<label>Lanceur enregistr&eacute;</label>
		        	</div>
		      	</div>
		    </div>
		</div>
		<div class="row">
			<div class="col m6 s12">
				<div class="row">
		        	<div class="input-field col s12">
		      			<select name="roll" id="roll">
		      				<option selected>100</option>
		      				<?php
		      				for($i = 90; $i > 0; $i -= 10){
		      					?>
		      					<option><?= $i ?></option>
		      					<?php
		      				}
		      				?>
		    			</select>
		    			<label>Valeur</label>
		    		</div>
		    	</div>
		    </div>
		    <div class="col m4 s12 center" style="margin-top: 2em">
		    	<label>
        			<input type="checkbox" name="hide" id="hide">
        			<span>Cacher le resultat</span>
      			</label>
		    </div>
		</div>
		<div class="row center">
			<div class="col s12">
				<button id="valid" type="button" class="waves-effect waves-light btn" style="margin-top: 1em"><i class="material-icons right">check</i>Valider</button>
			</div>
		</div>
	</form>
	<div class="row">
		<div class="col s12 center">
			<h3><i>Historique</i></h3>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<table class="responsive-table">
				<thead>
					<tr>
						<td><b>Lanceur</b></td>
						<td><b>Valeur</b></td>
						<td><b>D&eacute;</b></td>
					</tr>
				</thead>
				<tbody id="tbody">
					<?php
					$roll = array_reverse($_SESSION['roll']);
					foreach ($roll as $val) {
						?>
						<tr>
							<td><?= $val['name'] ?></td>
							<td><?= $val['roll'] ?></td>
							<td><?= $val['dice'] ?></td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</main>
<input type="hidden" id="msg" data-type="<?= $type ?>" value="<?= $value ?>">

<?php
require 'footer.php';